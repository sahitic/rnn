#%%
#importing the required libraries, modules etc.

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import Dense
from tensorflow.keras import regularizers
from tensorflow.keras import initializers
from sklearn.metrics import confusion_matrix

from tensorflow.keras.callbacks import TensorBoard
from tensorflow.keras.callbacks import EarlyStopping

import time

#%%
#importing state and action from behavioralSimulation
#data dimensions: conditions(3), rats(6), trials(200), features(2)

from behaviouralSimulation import state
from behaviouralSimulation import action
from behaviouralSimulation import reward
from behaviouralSimulation import corrupted_output

#%%
#specifying training and test data

training_input = np.concatenate((state[2, 0:4, :, :], np.concatenate((np.zeros((2,1,1)), 
    reward[2,0:4,0:-1,:]), axis = 1), np.concatenate((np.zeros((2,1,2)), 
    action[2,0:4, 0:-1,:]), axis = 1)), axis = 2)
training_output = corrupted_output[2, 0:4, :, :]

validation_input =  np.concatenate((state[2, 2:4, :, :], np.concatenate((np.zeros((2,1,1)), 
    reward[2,2:4,0:-1,:]), axis = 1), np.concatenate((np.zeros((2,1,2)), 
    action[2,2:4, 0:-1,:]), axis = 1)), axis = 2)
validation_output =  corrupted_output[2, 2:4, :, :]

test_input =  np.concatenate((state[2, 4:6, :, :], np.concatenate((np.zeros((2,1,1)), 
    reward[2,4:6,0:-1,:]), axis = 1), np.concatenate((np.zeros((2,1,2)), 
    action[2,4:6, 0:-1,:]), axis = 1)), axis = 2)
test_output =  corrupted_output[2, 4:6, :, :]

#%%
#creating NN model
model = tf.keras.Sequential()
model.add(LSTM(5, use_bias = True, return_sequences = True, input_shape= 
    (np.shape(training_input)[1] , np.shape(training_input)[2]),
    kernel_initializer = initializers.glorot_uniform(seed = 1), recurrent_initializer
    = initializers.Orthogonal(seed = 1)))
model.add(Dense(np.shape(training_output)[2], use_bias = True, 
    activation = 'softmax',  kernel_initializer = initializers.glorot_uniform(seed = 1)))
model.compile(loss='binary_crossentropy', optimizer='adam', metrics = ['accuracy'])
model.save_weights('model.h5') #model.h5 is the initial model

#%%

#getting loss and accuracy across different no. of training epochs

start_time = time.time()
score = np.full((15,2), np.nan)
for i in range(0,15):
    model.load_weights('model.h5')
    history = model.fit(training_input, training_output, epochs = 100*i, shuffle = False)
    score[i,0], score[i,1] = model.evaluate(validation_input, validation_output)
print("--- %s seconds ---" % (time.time() - start_time))

plt.figure()
plt.plot(range(0,1500,100), score[:,0])

#%%
#early stopping for regularisation
start_time = time.time()
model.load_weights('model.h5')
es = EarlyStopping(monitor = 'val_loss', mode = 'min', verbose = 1, patience = 50, 
                   min_delta = 0.001)
tensorboard = TensorBoard(log_dir='./logs')
model.fit(training_input, training_output, validation_data = (validation_input,
        validation_output), epochs = 1500, shuffle = False, callbacks = [es, tensorboard])
print("--- %s seconds ---" % (time.time() - start_time))


#%%
#training the model
start_time = time.time()
#tensorboard = TensorBoard(log_dir='./logs')
history = model.fit(training_input, training_output, epochs = 400, validation_data = 
        (np.reshape(validation_input[0,:,:], (1,200,5)),np.reshape(validation_output[0,:,:], (1,200,2))),
        shuffle = False) 
print("--- %s seconds ---" % (time.time() - start_time))
#callbacks=[tensorboard]

#%%
#predicting on test data with final model

predicted_output = model.predict(np.reshape(test_input[0,:,:], (1,200,5)))
score1, score2 = model.evaluate(np.reshape(test_input[0,:,:], (1,200,5)), 
        np.reshape(test_output[0,:,:], (1,200,2))) #loss, followed by accuracy

#%%
#some plotting to visualise results
plt.figure()
plt.hist(predicted_output[0,(test_input[0,:,0] == 0),0], label = 'when input = 0')
plt.hist(predicted_output[0,(test_input[0,:,0] == 1),0], label = 'when input = 1')
plt.legend(loc = 'upper right')
plt.title('Hist of predicted output (wrt input)')
plt.ylabel('Number of trials')
plt.xlabel('Probability of being in class 1')

plt.figure()
plt.hist(predicted_output[0,(test_output[0,:,0] == 0),0], label = 'when output = 0')
plt.hist(predicted_output[0,(test_output[0,:,0] == 1),0], label = 'when output = 1')
plt.legend(loc = 'upper right')
plt.title('Hist of predicted output (wrt output)')
plt.ylabel('Number of trials')
plt.xlabel('Probability of being in class 1')

plt.figure()
plt.boxplot([predicted_output[0,(test_input[0,:,0] == 0),0], 
        predicted_output[0,(test_input[0,:,0] == 1),0]], positions = [0,1])
plt.title('Boxplot of predicted output (wrt input)')
plt.ylabel('Probability of being in class')
plt.xlabel('Class label')

plt.figure()
plt.boxplot([predicted_output[0,(test_output[0,:,0] == 0),0],
        predicted_output[0,(test_output[0,:,0] == 1),0]],positions = [0,1])
plt.title('Boxplot of predicted output (wrt output)')
plt.ylabel('Probability of being in class')
plt.xlabel('Class label')

plt.figure()
plt.plot(test_input[0,:,0], label = 'input')
plt.plot(predicted_output[0,:,0], label = 'predicted_output')
plt.legend(loc = 'upper right')
plt.title('Predicted output and input in a sequence of test trials')
plt.ylabel('Probability of being in class 1')
plt.xlabel('Trial number')

plt.figure()
plt.plot(test_output[0,:,0], label = 'output')
plt.plot(predicted_output[0,:,0], label = 'predicted_output')
plt.legend(loc = 'upper right')
plt.title('Predicted output and output in a sequence of test trials')
plt.ylabel('Probability of being in class 1')
plt.xlabel('Trial number')

#%%
#confusion matrices to quantify performance
cm_score = predicted_output[0,:,0]>0.5
cm_output = confusion_matrix(test_output[0,:,0], cm_score)
cm_input = confusion_matrix(test_input[0,:,0], cm_score)

#%%
#ROC analysis
hits = np.full((20,1), np.nan)
false_alarms = np.full((20,1), np.nan)
for i,boundary in enumerate(np.linspace(np.min(predicted_output),
        np.max(predicted_output),num =  20)):
    hits[i,0] = np.sum(np.logical_and(predicted_output[0,:,0] > boundary, 
        test_output[0,:,0] == 1 ))/np.sum(test_output[0,:,0] == 1)
    false_alarms[i,0] = np.sum(np.logical_and(predicted_output[0,:,0] > boundary, 
        test_output[0,:,0] == 0))/np.sum(test_output[0,:,0] == 0)

plt.figure()
plt.plot(false_alarms, hits)
plt.title('Hits vs False Alarms')
plt.xlabel('False Alarms')
plt.ylabel('Hits')

#quantifying AUC
auc = -np.trapz(np.transpose(hits), np.transpose(false_alarms))


