#%%
import numpy as np
import matplotlib.pyplot as plt

np.random.seed(0)

state = np.full((3,6,200,2),np.nan) # 3 conditions; 6 rats; 100 trials; 1 inputs;
corrupted_output = np.full((3,6,200,2),np.nan) # 3 conditions; 6 rats; 200 trials; 2 outputs;
action = np.full((3,6,200,2),np.nan)
reward = np.full((3,6,200,1),np.nan)

for rat in range(np.shape(state)[1]):
    # random sequence
    state[0,rat,:,0] = np.random.binomial(n=1, p=.5, size=np.shape(state)[2])
    
    # continuity (1) and switching (2)
    state[1:3,rat,0,0] = np.random.binomial(n=1,p=.5,size=2)
    action[:,rat,0,0] = np.random.binomial(n=1,p=.5,size=3)
    
    
    for condition in range(np.shape(state)[0]):
        if action[condition, rat, 0, 0] == state[condition, rat, 0, 0]:
                reward[condition, rat, 0, 0] = 1        
        else:                
                reward[condition, rat, 0, 0] = 0

          

    for trial in range(1,np.shape(state)[2]):
        if np.random.rand(1)>.2:
           state[1,rat,trial,0] = state[1,rat,trial-1,0]
        else:
           state[1,rat,trial,0] = 1 - state[1,rat,trial-1, 0]

        if np.random.rand(1)>0.8:
            state[2, rat, trial, 0] = state[2, rat, trial - 1, 0]
        else:
            state[2, rat, trial, 0] = 1 - state[2, rat, trial - 1, 0]
            
        for condition in range(np.shape(state)[0]):
                       
            if action[condition, rat, trial-1, 0] == state[condition, rat, trial-1, 0]:
                reward[condition, rat, trial, 0] = 1
                action[condition, rat, trial, 0] = action[condition, rat, trial-1, 0]
            else:
                action[condition, rat, trial, 0] = 1 - action[condition, rat, trial-1, 0] 
                reward[condition, rat, trial, 0] = 0


#corruption from input to output
corrupted_output[:,:,:,0] = np.abs(state[:,:,:,0] - np.random.binomial(n=1,p=0.2,size=np.shape(state[:,:,:,0])))

#creating 2 input and 2 output modules
state[:,:,:,1] = 1 - state[:,:,:,0]
corrupted_output[:,:,:,1] = 1 - corrupted_output[:,:,:,0]
action[:,:,:,1] = 1 - action[:,:,:,0]



#%%
#visualisation of data

#visualising correlation in data
plt.figure()
plt.plot(np.correlate(state[2,0,:,0],state[2,0,:,0], mode = 'full')) 

plt.figure()
plt.plot(state[0,0,0:100,0], 'ro')
plt.figure()
plt.plot(reward[0,0,0:100,0], 'ro')
plt.figure()
plt.plot(action[0,0,0:100,0], 'ro')

print(np.correlate(state[0,0,:,0],action[0,0,:,0], mode = 'valid'))
print(np.correlate(state[0,0,:,0],state[0,0,:,0], mode = 'valid'))
print(np.correlate(state[0,0,:,0],state[0,1,:,0], mode = 'valid'))

