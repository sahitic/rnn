#gen inout/output but with block specific features

#%%
#importing libraries
import numpy as np

#%%
#generating inputs of 1's and 0's with prob of 0.5 each (randomly)
#then introduces dependency on prev input

np.random.seed(0)

input = np.random.binomial(size = (100, 1), n = 1, p=0.5)
print(input)

t = 0.2 #prob that t+1th input is different from tth input

for i in range(0, len(input)-1):
    b = np.random.binomial(size =1, n =1, p= t) #p=t is the prob of getting 1's
    print(b)
    if b == 0:
        input[i+1, 0] = input[i]
    else:
        input[i+1, 0] = 1 - input[i]
    
p = 0.0 #corruption probability
#bernoulli array with 1's with prob p, cane be used for output
a = np.random.binomial(size = (len(input), 1), n = 1, p=p) #array st 1 is found with prob p from bernoulli dist
output = np.copy(input);

#%%
#getting output from input using bernoulli array
for i, n in enumerate(a):
    if n == 1:
        output[i, 0] = 1 - output[i, 0]
        

