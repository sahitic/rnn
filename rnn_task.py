#%%
#importing the required libraries, modules etc.

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import sklearn as sl
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import Dense
from tensorflow.keras import regularizers
from tensorflow.keras import initializers
from sklearn.metrics import confusion_matrix

from tensorflow.keras.callbacks import TensorBoard

#%%
#importing stimulus and response from behavioralSimulation
#data dimensions: conditions(3), rats(6), trials(200), features(2)

from behaviouralSimulation import stimulus
from behaviouralSimulation import response
from behaviouralSimulation import outcome

#%%
 #length and train proportions(for a test-train split)
 
#reshaping the stimulus and response data for training and testing
#can choose a particular condition/ rat/ specific trials etc
x_train = np.reshape(stimulus[0, 0:2, :, :], (2,200,2))
y_train = np.reshape(response[0, 0:2, :, :], (2,200,2))

x_test = np.reshape(stimulus[0, 3:4, :, :], (1,200,2))
y_test =  np.reshape(response[0, 3:4, :, :], (1,200,2))
                 
#%%
#creating the model

model = tf.keras.Sequential()
model.add(LSTM(5, use_bias = True, return_sequences = True, batch_input_shape= (2,200,2), kernel_initializer = initializers.glorot_uniform(seed = 1), recurrent_initializer= initializers.Orthogonal(seed = 1)))
model.add(Dense(2, use_bias = True, activation = 'softmax',  kernel_initializer = initializers.glorot_uniform(seed = 1)))
model.compile(loss='binary_crossentropy', optimizer='adam', metrics = ['accuracy'])

#%%
#fitting the model

#tensorboard = TensorBoard(log_dir='./logs2')
history = model.fit(x_train, y_train, epochs = 10, shuffle = False) #callbacks=[tensorboard])

#%%
#copying training weights to an identical new model for prediction; compile new model

pred_model = tf.keras.Sequential()
pred_model.add(LSTM(5, use_bias = True, return_sequences = True, batch_input_shape= (1, 200, 2), kernel_initializer = initializers.glorot_uniform(seed = 1), recurrent_initializer= initializers.Orthogonal(seed = 1)))
pred_model.add(Dense(2, use_bias = True, activation = 'softmax',  kernel_initializer = initializers.glorot_uniform(seed = 1)))

old_weights = model.get_weights()
pred_model.set_weights(old_weights)
pred_model.summary()
pred_model.compile(loss='binary_crossentropy', optimizer='adam', metrics = ['accuracy'])

#%%
#validation with test set

score1 = pred_model.evaluate(x_test, y_test)
score = pred_model.predict(x_test)


