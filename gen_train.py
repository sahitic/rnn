#generating input stim and corresponding response (mice behavioral task) : completely randomised

#%%
#importing libraries
import numpy as np

#%%
#generating inputs of 1's and 0's from random uniform distribution

np.random.seed(0)

input = np.random.binomial(size = (1200, 1) , n = 1, p=0.5)

p = 0.0 #corruption probability

#bernoulli array with 1's with prob p, can be used for output
a = np.random.binomial(size = (len(input), 1), n = 1, p=p) #array st 1 is found with prob p from bernoulli dist
output = np.copy(input);

#%%
#getting output from input using bernoulli array
for i, n in enumerate(a):
    if n == 1:
        output[i, 0] = 1 - output[i, 0]
        

