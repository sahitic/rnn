import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.keras import layers 

training_data_x = np.array([[0,0], [1,0], [0,1], [1,1]]);
training_data_y = np.array([[0], [1], [1], [0]]);

model = tf.keras.Sequential()
model.add(layers.Dense(8, input_shape = (2,), activation = 'relu', use_bias =True))
model.add(layers.Dense(8, activation = 'relu', use_bias =True))
model.add(layers.Dense(1, activation = 'sigmoid'))

model.compile(optimizer='adam',
              loss='binary_crossentropy',
              metrics=['accuracy'])
 
callbacks = [
    tf.keras.callbacks.History()
]             

hist = model.fit(training_data_x, training_data_y, epochs =100, callbacks = callbacks);
