#%%
#importing the required libraries, modules etc.

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import sklearn as sl
from tensorflow.keras import layers 
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import Dense
from tensorflow.keras import regularizers
from tensorflow.keras import initializers
from sklearn.metrics import confusion_matrix

from tensorflow.keras import callbacks
from tensorflow.keras.callbacks import TensorBoard

#%%
#importing stimulus and response from behavioralSimulation
#data dimensions: condition(3), rats(6), trials(200), features(2)

from behaviouralSimulation import stimulus
from behaviouralSimulation import response

#%%
#length and train proportions(for a test-train split)

le = stimulus.shape[2] #no. of samples in each condition (for each rat)
trprop = int(0.6*le) #proportion of train values

#reshaping the stimulus and response data for training and testing
#can choose a particular condition/ rat/ specific trials etc
x_train = stimulus[0, 0, 0: trprop, 0].reshape(trprop,1,1)
x_test = stimulus[0, 0, trprop:le, 0].reshape(le-trprop,1,1)

y_train = response[0, 0, 0:trprop, :].reshape(1*trprop,2)
y_test = response[0, 0, trprop:le, :].reshape(1*(le-trprop),2)

#%%
#creating the model

model = tf.keras.Sequential()
model.add(LSTM(5, use_bias = True, return_sequences = True, batch_input_shape= (trprop,1,1), kernel_initializer = initializers.glorot_uniform(seed = 1), recurrent_initializer= initializers.Orthogonal(seed = 1)))
model.add(Dense(2, use_bias = True, activation = 'softmax',  kernel_initializer = initializers.glorot_uniform(seed = 1)))
model.compile(loss='binary_crossentropy', optimizer='adam', metrics = ['accuracy'])

#%%
#fitting the model

#tensorboard = TensorBoard(log_dir='./logs2')
history = model.fit(x_train, y_train, epochs = 10, shuffle = False) #callbacks=[tensorboard])

#%%
#copying training weights to an identical new model for prediction; compile new model

pred_model = tf.keras.Sequential()
pred_model.add(LSTM(5, use_bias = True, batch_input_shape= (le-trprop, 1, 1), kernel_initializer = initializers.glorot_uniform(seed = 1), recurrent_initializer= initializers.Orthogonal(seed = 1)))
pred_model.add(Dense(1, use_bias = True, activation = 'softmax',  kernel_initializer = initializers.glorot_uniform(seed = 1)))

old_weights = model.get_weights()
pred_model.set_weights(old_weights)
pred_model.summary()
pred_model.compile(loss='binary_crossentropy', optimizer='adam', metrics = ['accuracy'])

#%%
#validation with test set

score = pred_model.predict(x_test)

#score2 = pred_model.evaluate(x_test, y_test)

plt.figure()
#plt.plot(y_test, 'bo')
plt.plot(score, 'ro')
plt.show()

#%%
score1 = (score<0.48) #true (or 1) if above the set value

#confusion matrices 
cm_y = confusion_matrix(y_test, score1) #wrt y_test (how many correct wrt mouse's performance)
cm = confusion_matrix(input[trp+n:le+n], score1) #wrt x_test (how many correct wrt stimulus)

y_testCopy = y_test.reshape(len(y_test),1)
x_testCopy = input[trp+n:le+n].reshape(len(x_test),1)

out = np.concatenate((x_testCopy, y_testCopy, score1), axis = 1) #copying test and predicted data to a file
np.savetxt('out.csv', out, delimiter = ',')


