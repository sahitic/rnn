#%%

#importing the required libraries, modules etc.

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import sklearn as sl
from sklearn.model_selection import KFold
from tensorflow.keras import layers 
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import Dense
from tensorflow.keras import regularizers
#%%

#reading data from a data file and copying it into an accessible form (a dataframe)

df = pd.read_csv('C:/Users/u1076367/Desktop/household_power_consumption.txt', sep=';', 
                 parse_dates={'dt' : ['Date', 'Time']}, infer_datetime_format=True, 
                 low_memory=False, na_values=['nan','?'], index_col='dt')

values = df.resample('D').mean()

#%%

#at this point, could plot raw data and find trends in it; helps preparing data and deciding what models to use


#%%

#preparing data into the form needed for keras LSTM cell, converting to an array,etc.
#also, nan to num, test-train splits, normalisation etc. should be done at this point

x_train = np.zeros((721, 2))
x_test = np.zeros((721, 2))

kf = KFold(n_splits = 2, shuffle = True)

i = 0
for train, test in kf.split(values.index, values.Global_active_power):
    x_train[:,i], x_test[:,i] = values.Global_active_power[train],  values.Global_active_power[test]
    i =  i+1
    
X_train_1 = np.nan_to_num(x_train[:,0].reshape(721, 1, 1))
X_train_2 = np.nan_to_num(x_train[:,1].reshape(721, 1, 1))

y_train_1 = np.nan_to_num(np.roll(x_train[:, 0], 1))
y_train_2 = np.nan_to_num(np.roll(x_train[:, 1], 1))
y_train_1[0,] = 0
y_train_2[0,] = 0

#%%

#creating model with layers etc and compiling, figure out input shape, and regularizers, dropout etc.

model = tf.keras.Sequential()
model.add(LSTM(5, use_bias = True, batch_input_shape=(721, 1, 1), recurrent_regularizer = regularizers.l1(0.01) , kernel_regularizer = regularizers.l2(0.01)))
model.add(Dense(1, activation = 'sigmoid'))
model.summary()
model.compile(loss='mean_squared_error', optimizer='adam')

#%%

#fitting model to training data, can do it with multiple training sets etc. also a good point to get training stats

history = model.fit(X_train_1, y_train_1, epochs= 1)


#%%

#preparing test data set 

X_test_1 = np.nan_to_num(x_test[:,0].reshape(721, 1, 1))
X_test_2 = np.nan_to_num(x_test[:,1].reshape(721, 1, 1))

y_test_1 = np.nan_to_num(np.roll(x_test[:, 0], 1))
y_test_2 = np.nan_to_num(np.roll(x_test[:, 1], 1))
y_test_1[0,] = 0
y_test_2[0,] = 0

#%%

#copying training weights to an identical new model for prediction; compile new model

n =721 #predicting set size : X_test_1
pred_model = tf.keras.Sequential()
pred_model.add(LSTM(5, use_bias = True, batch_input_shape=(n, 1, 1), recurrent_regularizer = regularizers.l1(0.01) , kernel_regularizer = regularizers.l2(0.01)))
pred_model.add(Dense(1, activation = 'sigmoid'))
old_weights = model.get_weights()
pred_model.set_weights(old_weights)
pred_model.summary()
pred_model.compile(loss='mean_squared_error', optimizer='adam')


#%%

#can get test stats, plot predicted vs actual y's, etc.

score = pred_model.predict(X_test_1)
score2 = pred_model.evaluate(X_test_1, y_test_1)

plt.plot(y_test_1)
plt.plot(score)
plt.show()
